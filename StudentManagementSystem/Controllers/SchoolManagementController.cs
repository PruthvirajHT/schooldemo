﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;
using StudentManagementSystem.Repository;
using StudentManagementSystem.Services;
using Microsoft.Data.SqlClient;
using System.Data;
using StudentManagementSystem.CustomExceptions;

namespace StudentManagementSystem.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class SchoolManagementController : ControllerBase
    {
        private readonly ISchoolService _schoolService;

        public SchoolManagementController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        [HttpPost("addschool")]
        public async Task<IActionResult> AddSchool(School school)
        {
            try
            {
                var data = await _schoolService.AddSchool(school);

                if(data>0)
                {
                    return Ok("Data inserted");

                }
                else
                {
                    return NotFound();
                }
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }
        [HttpPost("addteacher")]
        public async Task<IActionResult> AddTeacher(Teacher teacher)
        {
            try
            {
                var data = await _schoolService.AddTeacher(teacher);

                if (data > 0)
                {
                    return Ok("Data inserted");

                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetSchoolDetails()
        {
            try
            {
                var documentaries = await _schoolService.GetSchoolDetails();
                if (documentaries == null)
                {
                    return NotFound();
                }

                return Ok(documentaries);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut("UpdateSchoolAddress")]

        public async Task<IActionResult> UpdateSchoolAddress(School school)
        {
            try
            {
                await _schoolService.UpdateSchoolAddress(school);
                return Ok("Data inserted");

            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        [HttpDelete("DeleteTeacher")]
        public async Task<IActionResult> DeleteTeacher(int? Id)
        {
           
            try
            {
                await _schoolService.DeleteTeacher(Id);
                return Ok("Teacher deleted");
            }

            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }

        }
        [HttpGet("{name}")]

        public async Task<IActionResult> GetTeacherListBySchoolName(string name)
        {
            
            try
            {
                
             var data =   await _schoolService.GetTeacherListBySchool(name);
                return Ok(data);
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }










    }
}



   
