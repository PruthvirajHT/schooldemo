﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagementSystem.CustomExceptions
{
    public class SqlCustomException :Exception
    {
        public SqlCustomException(String message, Exception exception) : base(message,exception)
        {

        }
    }
}
