﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagementSystem.Models
{
    public class School
    {
        public int SchoolId { set; get; }

        public string SchoolName { set; get; }

        

        public  ICollection<Teacher> Teachers { set; get; }
    }
}
