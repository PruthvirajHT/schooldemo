﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagementSystem.Models
{
    public class Subject
    {
        public int SubjectId { set; get; }

        public string SubjectName { set; get; }
    }
}
