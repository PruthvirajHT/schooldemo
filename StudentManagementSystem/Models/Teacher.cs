﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagementSystem.Models
{
    public class Teacher
    {
        public int TeacherId { set; get; }

        public string TeacherName { set; get; }

        public int SubjectId { set; get; }
        public Subject Subject { set; get; }

        public int SchoolId { set; get; }

        public School School { set; get; }
    }
}
