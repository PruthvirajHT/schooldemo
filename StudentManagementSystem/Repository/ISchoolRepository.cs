﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;

namespace StudentManagementSystem.Repository
{
    public interface ISchoolRepository
    {
        Task<int> AddSchool(School school);

        Task<int> AddTeacher(Teacher teacher);

        Task UpdateSchoolAddress(School school);
        Task<dynamic> GetSchoolDetails();

        Task<List<string>> GetTeacherListBySchool(string name);

        Task<int> DeleteTeacher(int? Id);


    }
}
