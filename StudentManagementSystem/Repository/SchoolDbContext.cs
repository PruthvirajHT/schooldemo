﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace StudentManagementSystem.Repository
{
    public class SchoolDbContext : DbContext
    {
        public SchoolDbContext()
        {

        }
            
        public SchoolDbContext(DbContextOptions<SchoolDbContext>options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SchoolEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherEntityConfiguration());

            modelBuilder.Entity<Subject>().HasData(
                new Subject
                {
                    SubjectId = 1,
                    SubjectName = "Biology"
                },
                new Subject
                {
                    SubjectId = 2,
                    SubjectName = "Zology"
                }
                );
        }

        public DbSet<School> Schools { set; get; }
        public DbSet<Teacher> Teachers { set; get; }
          
    }
}
