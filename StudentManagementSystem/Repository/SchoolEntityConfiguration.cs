﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace StudentManagementSystem.Repository
{
    public class SchoolEntityConfiguration :IEntityTypeConfiguration<School>
    {
        public void Configure(EntityTypeBuilder<School> builder)
        {
            builder.HasKey(p => p.SchoolId);
            builder.Property(a => a.SchoolId).
                UseIdentityColumn();
            builder.HasMany(a => a.Teachers).
                WithOne(p => p.School).HasForeignKey(a => a.TeacherId).IsRequired(true);
        }
    }
}
