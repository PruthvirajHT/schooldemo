﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;
using Microsoft.Data.SqlClient;
using StudentManagementSystem.CustomExceptions;
using Microsoft.EntityFrameworkCore;
using System.Data;
using StudentManagementSystem.Repository;

namespace StudentManagementSystem.Repository
{
    public class SchoolRepository :ISchoolRepository
    {
        private SchoolDbContext _schoolDbContext;

        public SchoolRepository(SchoolDbContext schoolDbContext)
        {
            _schoolDbContext = schoolDbContext;
        }

        public async Task<int> AddSchool(School school)
        {
           try
            {
                 await _schoolDbContext.AddAsync(school);
                await _schoolDbContext.SaveChangesAsync();
                return school.SchoolId;
            }
            catch(SqlException ex)
            {
                throw new SqlCustomException("Can't connect to database",ex);
            }
        }
        public async Task<int> AddTeacher(Teacher teacher)
        {
            try
            {
                await _schoolDbContext.AddAsync(teacher);
                await _schoolDbContext.SaveChangesAsync();
                return teacher.TeacherId;
            }
            catch (SqlException ex)
            {
                throw new SqlCustomException("Can't connect to database", ex);
            }
        }
        public async Task<dynamic> GetSchoolDetails()
        {
            try
            {
                if (_schoolDbContext != null)
                {
                    return await (from p in _schoolDbContext.Schools
                                  select new
                                  {
                                      SchoolId = p.SchoolId,
                                      SchoolName = p.SchoolName,
                                      
                                  }).ToListAsync();
                }

                return null;
            }
            catch (SqlException ex)
            {
                throw new SqlCustomException("Error can't connect to database", ex);
            }
        }

        public async Task UpdateSchoolAddress(School school)
        {
            if(_schoolDbContext!=null)
            {
                _schoolDbContext.Schools.Update(school);
                await _schoolDbContext.SaveChangesAsync();
            }
        }
      public async Task<List<string>> GetTeacherListBySchool(string name)
       {
            if (_schoolDbContext != null)
            {
                return await (from t in _schoolDbContext.Teachers
                              join s in _schoolDbContext.Schools on t.SchoolId equals s.SchoolId
                              where s.SchoolName == name
                              select t.TeacherName
                             ).ToListAsync();
               
            }
            return null;
       }
      public async Task<int> DeleteTeacher(int? Id)
      {
            int data = 0;

            if (_schoolDbContext != null)
            {
                var post = await _schoolDbContext.Teachers.FirstOrDefaultAsync(y => y.TeacherId == Id);

                if(post!=null)
                {
                    _schoolDbContext.Teachers.Remove(post);

                    data = await _schoolDbContext.SaveChangesAsync();
                }
                return data;
            }
            return data;

      }
    }
}
