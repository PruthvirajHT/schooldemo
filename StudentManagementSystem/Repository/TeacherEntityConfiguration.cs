﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace StudentManagementSystem.Repository
{
    public class TeacherEntityConfiguration :IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.HasKey(f => f.TeacherId);
            builder.Property(a => a.TeacherId).
                UseIdentityColumn();

            builder.Property(g => g.TeacherName).
                HasMaxLength(40).
                IsRequired();

            builder.HasOne(k => k.Subject).
                WithMany().
                HasForeignKey(f => f.SubjectId).
                IsRequired(true);

            builder.HasOne(h => h.School).
                WithMany(l => l.Teachers).
                IsRequired(true).
                HasForeignKey(p => p.SchoolId);
           
        }
    }
}
