﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Models;

namespace StudentManagementSystem.Services
{
    public interface ISchoolService
    {
        Task<int> AddSchool(School school);

        Task<int> AddTeacher(Teacher teacher);
        Task<dynamic> GetSchoolDetails();

        Task UpdateSchoolAddress(School school);

        Task<List<string>> GetTeacherListBySchool(string name);

        Task<int> DeleteTeacher(int? Id);
    }
}
