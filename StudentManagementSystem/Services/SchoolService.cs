﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentManagementSystem.Repository;
using StudentManagementSystem.Models;

namespace StudentManagementSystem.Services
{
    public class SchoolService : ISchoolService
    {
        private ISchoolRepository _IschoolRepository; 

        public SchoolService(ISchoolRepository IschoolRepository)
        {
            _IschoolRepository = IschoolRepository;
        }
        public Task<int> AddSchool(School school)
        {
            return _IschoolRepository.AddSchool(school);
        }

        public Task<int> AddTeacher(Teacher teacher)
        {
            return _IschoolRepository.AddTeacher(teacher);
        }
        public Task<dynamic> GetSchoolDetails()
        {
            return _IschoolRepository.GetSchoolDetails();
        }

        public Task UpdateSchoolAddress(School school)
        {
            return _IschoolRepository.UpdateSchoolAddress(school);
        }

        public Task<List<string>> GetTeacherListBySchool(string name)
        {
            return _IschoolRepository.GetTeacherListBySchool(name);
        }

        public Task<int> DeleteTeacher(int? Id)
        {
            return _IschoolRepository.DeleteTeacher(Id);
        }

    }
    
}
